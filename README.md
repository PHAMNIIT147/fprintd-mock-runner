### fprintd-mock-runner

A simple simulator for the [fprintd](https://gitlab.freedesktop.org/libfprint/fprintd) [DBus API](https://fprint.freedesktop.org/fprintd-dev/ref-dbus.html) that allows to mock device events in order to test a fprintd-based user interface (right now mostly tied to [gnome-control-center](https://salsa.debian.org/gnome-team/gnome-control-center/)

![](https://i.imgur.com/2B7I8kn.png)
